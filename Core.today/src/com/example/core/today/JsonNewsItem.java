package com.example.core.today;

import java.util.ArrayList;




public class JsonNewsItem {
	private String title;
	private String link;
	private String description;
	
	/**
	 * Initialize with icon and data array
	 */
	public JsonNewsItem() {
		
	}
	
	public JsonNewsItem(String a) {
		
	}

	/**
	 * Initialize with icon and strings
	 */
	public JsonNewsItem(String a, String b, String c) {
		this.title = a;
		this.link =b;
		this.description = c;
		
	}
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * Compare with the input object
	 * 
	 * @param other
	 * @return
	 */
	public boolean compareTo(JsonNewsItem other) {
		if (title.equals(other.getTitle())) {
			return false;
		} else if (link.equals(other.getLink())) {
			return false;
		} else if (description.equals(other.getDescription())) {
			return false;
		} 
		
		return true;
	}

}
